
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.Vector;

/**
 * Tato třída je určena k demostraci toho jak vytvořit prázdný obrázek, jak měnit
 * jednotlivé pixely v obrázku a jak obrázek zobrazit na obrazovce
 * při změně nějakých parametrů (v tomto případě při změně pozice na posuvníku).
 *
 * @author Ladislav Čmolík
 */
public class ImageDisplay extends JPanel implements ChangeListener, ActionListener {

    // Konstanty určující velikost obrázku
    private final int IMG_WIDTH = 500;
    private final int IMG_HEIGHT = 500;

    private Matrix matrix;
    private Matrix palette;

    private int sliderValue = 0;
    private String orientation = "z";
    private JTextField xTextField, yTextField, zTextField;
    private Vector<Double> normal = new Vector<>(3);
    private Vector<Integer> normalPoint = new Vector<>(3);

    // Obrázek
    BufferedImage image;
    // Rastr obrázku
    WritableRaster raster;

    public Matrix getMatrix() {
        return matrix;
    }

    public void setMatrix(Matrix matrix) {
        this.matrix = matrix;
    }

    public Matrix getPalette() {
        return palette;
    }

    public void setPalette(Matrix palette) {
        this.palette = palette;
    }

    public String getOrientation() {
        return orientation;
    }

    /**
     * definuje pohled podle os - nastavuje potrebne body pro vypocet smeroveho vektoru pohledu
     * @param orientation
     */
    public boolean setOrientation(String orientation) {

        switch (orientation) {
            case "z":
                xTextField.setText("0");
                yTextField.setText("0");
                zTextField.setText("1");
                this.orientation = orientation;
                break;
            case "x":
                xTextField.setText("0");
                yTextField.setText("1");
                zTextField.setText("0");
                this.orientation = orientation;
                break;
            case "y":
                xTextField.setText("255");
                yTextField.setText("0.01");
                zTextField.setText("0");
                this.orientation = orientation;
                break;
            default:
                return false;
        }

        return true;
    }

    /**
     * Vytvoří prázdný RGB obrázek kde na každý kanál jednoho pixelu připadá 8 bitů,
     * získá z obrázku rastr a nastaví rozměry JPanelu na velikost obrázku.
     */
    public ImageDisplay() {
        image = new BufferedImage(IMG_WIDTH, IMG_HEIGHT,
                BufferedImage.TYPE_4BYTE_ABGR);
        raster = image.getRaster();

        Dimension dim = new Dimension(IMG_WIDTH, IMG_HEIGHT);
        setMinimumSize(dim);
        setPreferredSize(dim);
        setMaximumSize(dim);
    }

    /**
     * Zapíše do každého pixelu displeje hodnoty x, y matice
     * Zároveň provede prevod hodnot barev z sede palety na barevnou
     */
    public void generateImage() {
        double[][][] displayMatrix = this.getMatrix().convertDataToDisplayMatrix(this.sliderValue, this.orientation, this.normal, this.palette);
        for(int x = 0; x < displayMatrix.length; x++) {
            for(int y = 0; y < displayMatrix[x].length; y++) {
                raster.setSample(x, y, 0, displayMatrix[x][y][0]);
                raster.setSample(x, y, 1, displayMatrix[x][y][1]);
                raster.setSample(x, y, 2, displayMatrix[x][y][2]);
                raster.setSample(x, y, 3, displayMatrix[x][y][3]);
            }
//            for(int y = displayMatrix[x].length; y < IMG_WIDTH; y++) {
//                raster.setSample(x, y, 0, 0);
//                raster.setSample(x, y, 1, 0);
//                raster.setSample(x, y, 2, 0);
//                raster.setSample(x, y, 3, 255);
//            }
        }
    }

    /**
     * Vykreslí obrázek do panelu.
     */
    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g.drawImage(image, 0, 0, this);
    }

    /**
     * Implementace rozhraní ChangeListener volá se vždy když se změní hodnota
     * na posuvníku. Na základě hodnoty na posuvníku je překreslen obsah obrázku
     * a obrázek je zobrazen.
     */
    public void stateChanged(ChangeEvent e) {
        JSlider slider = (JSlider) e.getSource();
        this.sliderValue = slider.getValue();
        generateImage();
        // Metoda repaint() způsobí, že se zavolá metoda paint() této třídy.
        repaint();
    }

    /**
     * implementace listeneru pro zmenu hodnoty radio buttonu
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        String axis = e.getActionCommand();
        this.setOrientation(axis);

        Double x = Double.parseDouble(xTextField.getText());
        Double y = Double.parseDouble(yTextField.getText());
        Double z = Double.parseDouble(zTextField.getText());

        this.normal.clear();
        this.normal.add(x);
        this.normal.add(y);
        this.normal.add(z);
        normalizeVector();

        generateImage();
        repaint();
    }

    /**
     * Hlavní metoda třídy. Vytvoří instanci této třídy, vytvoří posuvník na
     * kterém je možné zadávat hodnoty od 0 do 255, nastaví instanci této třídy
     * jako listener na změnu hodnoty na posuvníku, vytvoří nové okno (JFrame) a
     * vloží do něj instanci této třídy a posuvník.
     */
    public void drawDisplay() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("The atempt to set system Look&Feel failed. " +
                    "Continuing with default.");
        }

//        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, IMG_WIDTH - 2, 0);
//        slider.setPaintTicks(true);
//        slider.addChangeListener(this);

        //Create the editable fields for entering the vector coordinates
        xTextField = new JTextField(2);
        JLabel xLabel = new JLabel("x: ");

        yTextField = new JTextField(2);
        JLabel yLabel = new JLabel("y: ");

        zTextField = new JTextField(2);
        JLabel zLabel = new JLabel("z: ");

        JButton button = new JButton("Display");

        JPanel vectorPanel = new JPanel(new GridLayout(1,0));
        vectorPanel.add(xLabel);
        vectorPanel.add(xTextField);
        vectorPanel.add(yLabel);
        vectorPanel.add(yTextField);
        vectorPanel.add(zLabel);
        vectorPanel.add(zTextField);
        vectorPanel.add(button);

        //Add event listener to the button
        button.addActionListener(this);

        //Create the radio buttons.
        JRadioButton xButton = new JRadioButton("X");
        xButton.setActionCommand("x");

        JRadioButton yButton = new JRadioButton("Y");
        yButton.setActionCommand("y");

        JRadioButton zButton = new JRadioButton("Z");
        zButton.setActionCommand("z");

        //Group the radio buttons.
        ButtonGroup group = new ButtonGroup();
        group.add(xButton);
        group.add(yButton);
        group.add(zButton);

        //Register a listener for the radio buttons.
        xButton.addActionListener(this);
        yButton.addActionListener(this);
        zButton.addActionListener(this);

        //Panel for radio button
        JPanel radioPanel = new JPanel(new GridLayout(1,0));
        radioPanel.add(xButton);
        radioPanel.add(yButton);
        radioPanel.add(zButton);

        JFrame frame = new JFrame("Image display");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(this, BorderLayout.CENTER);
//        frame.getContentPane().add(slider, BorderLayout.SOUTH);
        frame.getContentPane().add(radioPanel, BorderLayout.EAST);
        frame.getContentPane().add(vectorPanel, BorderLayout.NORTH);
        frame.pack();
        frame.setVisible(true);

        // bod, na ktery se divame;
        xTextField.setText("0");
        yTextField.setText("1");
        zTextField.setText("1");
        this.normal.add(0.0);
        this.normal.add(1.0);
        this.normal.add(1.0);

        normalizeVector();

        generateImage();
        repaint();
    }

    // normalizuje normalovy vektor
    private void normalizeVector() {
        // normalizace normaly
        double V = Math.sqrt(this.normal.get(0)*this.normal.get(0) + this.normal.get(1)*this.normal.get(1) + this.normal.get(2)*this.normal.get(2));
        this.normal.set(0, normal.get(0) / V);
        this.normal.set(1, normal.get(1) / V);
        this.normal.set(2, normal.get(2) / V);
    }
}