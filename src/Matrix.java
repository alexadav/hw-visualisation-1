import java.util.Vector;

/**
 * Created by info on 10.03.16.
 */
public class Matrix {
    private final int DATA_X = 256;
    private final int DATA_Y = 256;
    private final int DATA_Z = 113;
    private final int DISPLAY_X = 500;
    private final int DISPLAY_Y = 500;
    private final int DISPLAY_Z = 500;
    private final int GREY_MAX = 3272;

    private double[][][] dataMatrix; // matice se zdrojovymi daty
    private double[][][] rotatedMatrix;
    private double[][] displayMatrix; // matice pro zobrazeni bodu na displeji

    double minX = 2000; double minY = 2000; double minZ = 2000;
    double maxX = -2000; double maxY = -2000; double maxZ = -2000;

    private Vector<Double> lastNormalVector;
    private LightModel lig;

    public Matrix() {}

    public double[][][] getDataMatrix() {
        return dataMatrix;
    }

    public void setDataMatrix(double[][][] dataMatrix) {
        this.dataMatrix = dataMatrix;
    }

    public double[][] getDisplayMatrix() {
        return displayMatrix;
    }

    public void setDisplayMatrix(double[][] displayMatrix) {
        this.displayMatrix = displayMatrix;
    }

    public void setDataMatrixValue(int x, int y, int z, double val) {
        this.dataMatrix[x][y][z] = val;
    }

    public double getDataMatrixValue(int x, int y, int z) {
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        if (z < 0) z = 0;
        if (x >= this.dataMatrix.length) x = this.dataMatrix.length - 1;
        if (y >= this.dataMatrix[x].length) y = this.dataMatrix[x].length - 1;
        if (z >= this.dataMatrix[x][y].length) z = this.dataMatrix[x][y].length - 1;
        return this.dataMatrix[x][y][z];
    }

    public double getRotatedMatrixValue(int x, int y, int z) {
        if (x < 0) x = 0;
        if (y < 0) y = 0;
        if (z < 0) z = 0;
        if (x >= this.rotatedMatrix.length) x = this.rotatedMatrix.length - 1;
        if (y >= this.rotatedMatrix[x].length) y = this.rotatedMatrix[x].length - 1;
        if (z >= this.rotatedMatrix[x][y].length) z = this.rotatedMatrix[x][y].length - 1;
        return this.rotatedMatrix[x][y][z];
    }

    public void setDisplayMatrixValue(int x, int y, double val) {
        this.displayMatrix[x][y] = val;
    }

    public double getDisplayMatrixValue(int x, int y) {
        if (x >= this.displayMatrix.length) x = this.displayMatrix.length - 1;
        if (y >= this.displayMatrix[x].length) y = this.displayMatrix[x].length - 1;
        return this.displayMatrix[x][y];
    }

    /**
     * Vypocita hodnotu barev matice pro zobrazeni na displeji. V prubehu aplikuje pohled na zaklade normaloveho vektoru.
     *
     * @param z
     * @param orientation
     * @param axisVector
     * @param normalVector
     * @param normalPoint
     */
    public double[][][] convertDataToDisplayMatrix(int z, String orientation, Vector<Double> normalVector, Matrix palette) {
        double[][][] displayMatrix = new double[DISPLAY_X][DISPLAY_Y][4];
        double[] source = {normalVector.get(0), normalVector.get(1), normalVector.get(2)};
        this.lig = new LightModel(source, 0.4, 0.6, 1.5);

        double[][] translateZ = {
                {1, 0, 0, -Math.round(DATA_X/2)},
                {0, 1, 0, -Math.round(DATA_Y/2)},
                {0, 0, 1, -Math.round(DATA_X/2)},
                {0, 0, 0, 1}
        };
        double[][] translateZ_i = {
                {1, 0, 0, Math.round(DATA_X/2)},
                {0, 1, 0, Math.round(DATA_Y/2)},
                {0, 0, 1, Math.round(DATA_X/2)},
                {0, 0, 0, 1}
        };

        // vypocet uhlu mezi dvema vektory - promitneme vektor na osu XY
        Vector<Double> axisVector = new Vector<>(4);
        double a = normalVector.get(0);
        double b = normalVector.get(1);
        double c = normalVector.get(2);
        double d = this.diagonaleSize(b, c);
        double cd = (d != 0) ? c/d : 0;
        double bd = (d != 0) ? b/d : 0;
        double[][] rotateX = {
                {1, 0, 0, 0},
                {0, cd, -bd, 0},
                {0, bd, cd, 0},
                {0, 0, 0, 1}
        };
        double[][] rotateX_i = {
                {1, 0, 0, 0},
                {0, cd, bd, 0},
                {0, -bd, cd, 0},
                {0, 0, 0, 1}
        };

        double dd = (d > 0) ? d : 1;
        double[][] rotateY = {
                {dd, 0, -a, 0},
                {0, 1, 0, 0},
                {a, 0, dd, 0},
                {0, 0, 0, 1}
        };
        double[][] rotateY_i = {
                {dd, 0, a, 0},
                {0, 1, 0, 0},
                {-a, 0, dd, 0},
                {0, 0, 0, 1}
        };

        axisVector.clear();
        axisVector.add(0.0);
        axisVector.add(0.0);
        axisVector.add(1.0);
        double t = this.getAngle(axisVector, normalVector);
        double cosT = Math.cos(t);
        double sinT = Math.sin(t);
        double[][] rotateZ = {
                {cosT, -sinT, 0, 0},
                {sinT, cosT, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
        };

        double mat[][];
        if (d != 0) {
            mat = this.multiply(translateZ_i, rotateX_i);
            mat = this.multiply(mat, rotateY_i);
            mat = this.multiply(mat, rotateZ);
            mat = this.multiply(mat, rotateY);
            mat = this.multiply(mat, rotateX);
            mat = this.multiply(mat, translateZ);
        } else {
            mat = this.multiply(translateZ_i, rotateZ);
            mat = this.multiply(mat, translateZ);
        }


//        if (1 || this.lastNormalVector != normalVector) {
            // zjistime minimalni a maximalni pozice bodu v krychli po aplikovani rotace
            for (int x = 0; x < DATA_X; x=x+5) {
                for (int y = 0; y < DATA_Y; y=y+5) {
                    for (int az = 0; az < DATA_Z; az=az+5) {
                        double[] aaa = {x, y, az, 0};
                        aaa = this.multiply(mat, aaa);
                        this.minX = Math.min(this.minX, aaa[0]);
                        this.minY = Math.min(this.minY, aaa[1]);
                        this.minZ = Math.min(this.minZ, aaa[2]);
                        this.maxX = Math.max(this.maxX, aaa[0]);
                        this.maxY = Math.max(this.maxY, aaa[1]);
                        this.maxZ = Math.max(this.maxZ, aaa[2]);
                    }
                }
            }

            this.lastNormalVector = normalVector;
//        }


        /** vypocitame obalovou kouli orotovane krychle **/
        // diagonala puvodni krychle. Prvne vypocitame uhlopricku podstavy, pak diagonalu krychle.
//        double diagCube = this.diagonaleSize(Math.sqrt(2 * DISPLAY_X * DISPLAY_X), DISPLAY_X);

        double sX = (this.maxX - this.minX);
        double sY = (this.maxY - this.minY);
        double sZ = (this.maxZ - this.minZ);

        this.rotatedMatrix = new double[(int)Math.ceil(sX)][(int)Math.ceil(sY)][(int)Math.ceil(sZ)];
        for (int x = 0; x < DATA_X; x++) {
            for (int y = 0; y < DATA_Y; y++) {
                for (z = 0; z < DATA_X; z++) { // chceme rotovat kostku
                    double[] aaa = {x, y, z, 0};
                    aaa = this.multiply(mat, aaa);
                    double tx = aaa[0] - this.minX;
                    double ty = aaa[1] - this.minY;
                    double tz = aaa[2] - this.minZ;
                    if (tx < sX && ty < sY && tz < sZ && tx > 0 && ty > 0 && tz > 0) {
                        this.rotatedMatrix[(int)tx][(int)ty][(int)tz] = this.getDataMatrixValue(x, y, (int)(z * DATA_Z / DATA_X));
                    }
                }
            }
        }

        // nyni pro orotovane body ziskame konkretni hodnoty barvy
        for (int x = 0; x < this.rotatedMatrix.length; x++) {
            for (int y = 0; y < this.rotatedMatrix[0].length; y++) {
                if (x >= 500 || y >= 500) continue;
                // generate samples in ray
                int arrSize = this.rotatedMatrix[0][0].length / 2;
                double[][] samples = new double[arrSize][4];
                for (z = 0; z < arrSize; z++) {
                    int zSample = z * 2;
//                    double[] aaa = {x, y, zSample, 0};
//                    aaa = this.multiply(mat, aaa);
//                    double tx = aaa[0] - this.minX;
//                    double ty = aaa[1] - this.minY;
//                    double tz = aaa[2] - this.minZ;
//                    double xx = tx * (DATA_X / sX);
//                    double yy = ty * (DATA_Y / sY);
//                    double zz = tz * (DATA_Z / sZ);
////                    double xx = aaa[0];
////                    double yy = aaa[1];
////                    double zz = aaa[2];
//
                    double val = this.interpolateRotatedMatrix(x, y, zSample); // barva v bode
//                    double val = this.interpolateDisplayMatrix(x, y, zSample); // barva v bode

//                    int paletteIndex = palette.interpolateColorToPaletteIndex(val); // index bodu v barevne palete
                    int paletteIndex = palette.interpolateColorToPaletteIndex(val); // index bodu v barevne palete

                    double palPoint[] = new double[4]; // barva z palety
                    palPoint[0] = palette.getDataMatrixValue(paletteIndex, 0, 0);
                    palPoint[1] = palette.getDataMatrixValue(paletteIndex, 0, 1);
                    palPoint[2] = palette.getDataMatrixValue(paletteIndex, 0, 2);
                    palPoint[3] = palette.getDataMatrixValue(paletteIndex, 0, 3) / 255;
//                    double[] point = this.phongPoint(xx, yy, zz, palPoint);
                    double[] point = this.phongPoint(x, y, zSample, palPoint);

                    // save in reverse
                    samples[arrSize - z - 1] = point;
                }

                displayMatrix[x][y][3] = 255;
                for (int i = 0; i < samples.length; i++) {
                    double in = 1;
//                    double zSample = i * 5;
//                    double[] aaa = {x, y, zSample, 0};
//                    aaa = this.multiply(mat, aaa);
//                    double tx = aaa[0] - this.minX;
//                    double ty = aaa[1] - this.minY;
//                    int xx = (int)(tx * (DATA_X / sX));
//                    int yy = (int)(ty * (DATA_Y / sY));

                    for (int j = i + 1; j < samples.length; j++) {
                        // 1 - alfa_j
                        double alphaj = samples[j][3];
                        in *= (1 - alphaj);
                    }
                    double alpha = samples[i][3];
                    displayMatrix[x][y][0] += (samples[i][0] * alpha) * in;
                    displayMatrix[x][y][1] += (samples[i][1] * alpha) * in;
                    displayMatrix[x][y][2] += (samples[i][2] * alpha) * in;
//                    displayMatrix[x][y][3] += (palette.getDataMatrixValue(paletteIndex, 0, 3) * alpha) * in;
                }



//                double val = this.interpolateDisplayMatrix(tx, ty, tz);
//                double val = this.interpolateDisplayMatrix(aaa[0], aaa[1], aaa[2]);
            }
        }
        return displayMatrix;
    }

    public double[] phongPoint(double x, double y, double z, double[] rgb) {
        Double[] grad = this.getGradient(x, y, z);
        this.normalize(grad);
//        this.lig.getPhong(grad[0], grad[1], grad[2], rgb);

        return rgb;
    }

    private Double[] normalize(Double[] v) {
        double vv = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
        if (vv != 0) {
            v[0] = v[0] / vv;
            v[1] = v[1] / vv;
            v[2] = v[2] / vv;
        } else {
            v[0] = 0.0;
            v[1] = 0.0;
            v[2] = 0.0;
        }


        return v;
    }



    // interpoluje souradnice bodu z matice pro zobrazeni na displeji a nacita odpovidajici hodnoty bodu z matice dat
    public double interpolateRotatedMatrix(double xx, double yy, double zz) {

        int x1 = (int)Math.floor(xx);
        int x2 = (int)Math.ceil(xx);
        int y1 = (int)Math.floor(yy);
        int y2 = (int)Math.ceil(yy);
        int z1 = (int)Math.floor(zz);
        int z2 = (int)Math.ceil(zz);

        // toto zabraňuje matematických chybám v zaokrouhlování a převodu (vznikaly chyby v řádech -1 na -3 apod)
        if (x1 < 0) x1 = 0;
        if (x2 < 0) x2 = 0;
        if (y1 < 0) y1 = 0;
        if (y2 < 0) y2 = 0;
        if (z1 < 0) z1 = 0;
        if (z2 < 0) z2 = 0;

        // pokud jsme v 0, tak jsou x1 i x2 stejne (nasobeni nulou vyse)
        if (x1 == x2) x2 = x1 + 1;
        if (y1 == y2) y2 = y1 + 1;
        if (z1 == z2) z2 = z1 + 1;

        double q000 = this.getRotatedMatrixValue(x1, y1, z1);
        double q001 = this.getRotatedMatrixValue(x1, y1, z2);
        double q010 = this.getRotatedMatrixValue(x1, y2, z1);
        double q011 = this.getRotatedMatrixValue(x1, y2, z2);
        double q100 = this.getRotatedMatrixValue(x2, y1, z1);
        double q101 = this.getRotatedMatrixValue(x2, y1, z2);
        double q110 = this.getRotatedMatrixValue(x2, y2, z1);
        double q111 = this.getRotatedMatrixValue(x2, y2, z2);

        double val = this.triLerp(xx, yy, zz, q000, q001, q010, q011, q100, q101, q110, q111, x1, x2, y1, y2, z1, z2);
        return val;
    };

    // interpoluje souradnice bodu z matice pro zobrazeni na displeji a nacita odpovidajici hodnoty bodu z matice dat
    public double interpolateDataMatrix(double xx, double yy, double zz) {

        int x1 = (int)Math.floor(xx);
        int x2 = (int)Math.ceil(xx);
        int y1 = (int)Math.floor(yy);
        int y2 = (int)Math.ceil(yy);
        int z1 = (int)Math.floor(zz);
        int z2 = (int)Math.ceil(zz);

        // toto zabraňuje matematických chybám v zaokrouhlování a převodu (vznikaly chyby v řádech -1 na -3 apod)
        if (x1 < 0) x1 = 0;
        if (x2 < 0) x2 = 0;
        if (y1 < 0) y1 = 0;
        if (y2 < 0) y2 = 0;
        if (z1 < 0) z1 = 0;
        if (z2 < 0) z2 = 0;

        // pokud jsme v 0, tak jsou x1 i x2 stejne (nasobeni nulou vyse)
        if (x1 == x2) x2 = x1 + 1;
        if (y1 == y2) y2 = y1 + 1;
        if (z1 == z2) z2 = z1 + 1;

        double q000 = this.getDataMatrixValue(x1, y1, z1);
        double q001 = this.getDataMatrixValue(x1, y1, z2);
        double q010 = this.getDataMatrixValue(x1, y2, z1);
        double q011 = this.getDataMatrixValue(x1, y2, z2);
        double q100 = this.getDataMatrixValue(x2, y1, z1);
        double q101 = this.getDataMatrixValue(x2, y1, z2);
        double q110 = this.getDataMatrixValue(x2, y2, z1);
        double q111 = this.getDataMatrixValue(x2, y2, z2);

        double val = this.triLerp(xx, yy, zz, q000, q001, q010, q011, q100, q101, q110, q111, x1, x2, y1, y2, z1, z2);
        return val;
    };

    // interpoluje pouze hodnotu indexu pro mapovani barvy (seda skala) na barevnou paletu nactenou ze souboru
    public int interpolateColorToPaletteIndex(double x) {
        double coefX = ((double)this.getDataMatrix().length / (double)GREY_MAX);

        double xx = x * coefX;
        int x1 = (int)Math.floor(xx);
        int x2 = (int)Math.ceil(xx);

        // toto zabraňuje matematických chybám v zaokrouhlování a převodu (vznikaly chyby v řádech -1 na -3 apod)
        if (x1 < 0) x1 = 0;
        if (x2 < 0) x2 = 0;

        // pokud jsme v 0, tak jsou x1 i x2 stejne (nasobeni nulou vyse)
        if (x1 == x2) x2 = x1 + 1;

        // vratime ten index, ktery je k hledane hodnote blize
        return (Math.abs(x1 - xx) >= Math.abs(x2 - xx)) ? x2 : x1;
    };

    /**
     * linear interpolation
     *
     * Used from https://gist.github.com/begla/1019993
     */
    public static double lerp(double x, double x1, double x2, double q00, double q01) {
        double d = (x - x1) / (x2 - x1);
        return (1 - d) * q00 + d * q01;
    }

    /**
     * bilinear interpolation
     *
     * Used from https://gist.github.com/begla/1019993
     */
    public static double biLerp(double x, double y, double q11, double q12, double q21, double q22, double x1, double x2, double y1, double y2) {
        double r1 = lerp(x, x1, x2, q11, q21);
        double r2 = lerp(x, x1, x2, q12, q22);

        return lerp(y, y1, y2, r1, r2);
    }

    /**
     * trilinear interpolation
     *
     * Based on https://en.wikipedia.org/wiki/Trilinear_interpolation
     */
    public static double triLerp(double x, double y, double z, double q000, double q001, double q010, double q011, double q100, double q101, double q110, double q111, double x1, double x2, double y1, double y2, double z1, double z2) {
        double x00 = lerp(x, x1, x2, q000, q100);
        double x01 = lerp(x, x1, x2, q001, q101);
        double x10 = lerp(x, x1, x2, q010, q110);
        double x11 = lerp(x, x1, x2, q011, q111);

        double r0 = lerp(y, y1, y2, x00, x10);
        double r1 = lerp(y, y1, y2, x01, x11);

        return lerp(z, z1, z2, r0, r1);
    }

    // vypocita velikost uhlopricky na zaklade dvou hran
    public double diagonaleSize(double a, double b) {
        return Math.sqrt(a * a + b * b);
    }

    // return C = A^T
    public static double[][] transpose(double[][] A) {
        int m = A.length;
        int n = A[0].length;
        double[][] C = new double[n][m];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                C[j][i] = A[i][j];
        return C;
    }

    public static double[][] inverse(double[][] A) {
        // Gaussian elimination with partial pivoting
        int N = A.length;
        for (int i = 0; i < N; i++) {

            // find pivot row and swap
            int max = i;
            for (int j = i + 1; j < N; j++)
                if (Math.abs(A[j][i]) > Math.abs(A[max][i]))
                    max = j;

            double[] temp = A[i];
            A[i] = A[max];
            A[max] = temp;

            // singular
            if (A[i][i] == 0.0) throw new RuntimeException("Matrix is singular.");

            // pivot within A
            for (int j = i + 1; j < N; j++) {
                double m = A[j][i] / A[i][i];
                for (int k = i+1; k < N; k++) {
                    A[j][k] -= A[i][k] * m;
                }
                A[j][i] = 0.0;
            }
        }

        return A;

    }

    // return C = A * B
    public static double[][] multiply(double[][] A, double[][] B) {
        int mA = A.length;
        int nA = A[0].length;
        int mB = B.length;
        int nB = B[0].length;
        double[][] C = new double[mA][nB];
        for (int i = 0; i < mA; i++)
            for (int j = 0; j < nB; j++)
                for (int k = 0; k < nA; k++)
                    C[i][j] += A[i][k] * B[k][j];
        return C;
    }

    // matrix-vector multiplication (y = A * x)
    public static double[] multiply(double[][] A, double[] x) {
        int m = A.length;
        int n = A[0].length;
//        if (x.length != n) throw new RuntimeException("Illegal matrix dimensions.");
        double[] y = new double[m];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                y[i] += A[i][j] * x[j];
        return y;
    }


    // vector-matrix multiplication (y = x^T A)
    public static double[] multiply(double[] x, double[][] A) {
        int m = A.length;
        int n = A[0].length;
//        if (x.length != m) throw new RuntimeException("Illegal matrix dimensions.");
        double[] y = new double[n];
        for (int j = 0; j < n; j++)
            for (int i = 0; i < m; i++)
                y[j] += A[i][j] * x[i];
        return y;
    }

    public static double getAngle(Vector<Double> v1, Vector<Double> v2) {
        double uv = Math.abs(v1.get(0) * v2.get(0) + v1.get(1) * v2.get(1) + v1.get(2) * v2.get(2));
        double uu = Math.sqrt(v1.get(0) * v1.get(0) + v1.get(1) * v1.get(1) + v1.get(2) * v1.get(2));
        double vv = Math.sqrt(v2.get(0) * v2.get(0) + v2.get(1) * v2.get(1) + v2.get(2) * v2.get(2));
        double a = Math.acos(uv / (uu * vv));

        return a;
    }

    public Double gradient(double x, double y, double z) {
        return this.getRotatedMatrixValue((int)Math.round(x), (int)Math.round(y), (int)Math.round(z));
    }

    public Double[] getGradient(double x, double y, double z) {
        double Dx, Dy, Dz;

        Dx = this.gradient(x - 1, y, z) - this.gradient(x + 1, y, z);
        Dy = this.gradient(x, y - 1, z) - this.gradient(x, y + 1, z);
        Dz = this.gradient(x, y, z - 1) - this.gradient(x, y, z + 1);

//        if (Dx > 0 || Dy > 0 || Dz > 0) {
//            int asf = 1;
//        }

        Double[] d = {Dx, Dy, Dz};

        return d;
    }

}

