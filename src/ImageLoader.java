import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.awt.Color;
import java.io.File;
import java.io.IOException;

/**
 * Ukázka načtení dat z 16 bitového PNG obrázku
 *
 * @author Ladislav Čmolík
 */
public class ImageLoader {

    /**
     * nacte postupne vsechny hodnoty pixelu z obrazku a ulozi je do matice
     * @return
     */
    public Matrix loadImages() {
        Matrix matrix = new Matrix();
        try {
            for (int i = 1; i <= 113; i++) {
                // Vytvoříme objekt File z cesty k obrázku
                String fileName = String.format("data/cthead/cthead-16bit%03d.png",
                        i);
                File path = new File(fileName);

                // Načteme obrázek. Pokud soubor neexistuje bude vyhozena
                // výjimka IOException
                BufferedImage image = ImageIO.read(path);

                // Získáme raster načteného obrázku
                WritableRaster raster = image.getRaster();

                if (i == 1) {
                    matrix.setDataMatrix(new double[image.getWidth()][image.getHeight()][113]);
                }

                // Projdeme všechny pixely obrázku a vypíšeme jejich hodnotu
                for (int x = 0; x < image.getWidth(); x++) {
                    for (int y = 0; y < image.getHeight(); y++) {
                        // Obrázek je ve stupních šedi (16 bitů na pixel),
                        // proto bereme pouze první kanál (třetí parametr je 0).
                        int value = raster.getSample(x, y, 0);
//                        System.out.printf("Hodnota na souřadnicích (%d, %d) je %d\n",
//                                x, y, value);
                        matrix.setDataMatrixValue(x, y, i - 1, value);
//                        System.out.printf("Hodnota na souřadnicích (%d, %d) je %d\n",
//                                x, y, this.matrix[x][y][i-1]);
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return matrix;
    }

    /**
     * nacita hodnoty barev z obrazku obsahujiciho barevnou paletu
     * zde je zmena, je potreba ukladat plne RGB
     *
     * @return
     */
    public Matrix loadPalette() {
        Matrix matrix = new Matrix();
        try {
            // Vytvoříme objekt File z cesty k obrázku
//            String fileName = "data/palette.png";
//            String fileName = "data/palette_skin.png";
            String fileName = "data/palette_kosti.png";
            File path = new File(fileName);

            // Načteme obrázek. Pokud soubor neexistuje bude vyhozena
            // výjimka IOException
            BufferedImage image = ImageIO.read(path);

            // Získáme raster načteného obrázku
//            WritableRaster alphaRaster = image.getAlphaRaster();
//            WritableRaster raster = image.getRaster();

            // budeme si ukladat hodnoty rgb jako hodnoty osy z
            matrix.setDataMatrix(new double[image.getWidth()][1][4]);

            // Projdeme všechny pixely obrázku a vypíšeme jejich hodnotu
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = 0; y < 1; y++) {
                    // Obrázek je v RGB, bereme postupně jednotlivé složky a ukládáme je jako bod z
                    Color c = new Color(image.getRGB(x, y), true);

                    matrix.setDataMatrixValue(x, y, 0, c.getRed());
                    matrix.setDataMatrixValue(x, y, 1, c.getGreen());
                    matrix.setDataMatrixValue(x, y, 2, c.getBlue());
                    // set alpha
                    matrix.setDataMatrixValue(x, y, 3, c.getAlpha());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return matrix;
    }
}