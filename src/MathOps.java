
/**
 * @brief Collection of rudimentary but common mathemathical calculations, processing
 * Also contains serialization of array numerics for flexible debugging, introspection
 *
 * @author Francis Kirigin
 *
 * $Id: MathOps.java,v 1.1 2006/12/04 08:15:58 ffk6042 Exp $
 */

import java.awt.*;
import java.text.DecimalFormat;

public class MathOps { // implements MathCommon {

    public DecimalFormat fmt = new DecimalFormat("#.0000");

    // DEBUG FLAGS
    boolean DBG_MODE = false, VERBOSE_MODE = false;


    /***********************************************************
     * CONSTRUCTORS: Potentially unnecessary constructors
     *
     ************************************************************/
    public MathOps() { }

    public MathOps(Point p)
    {
        Point origin = p;
    }

    /* Compute value of interpolation given factor 't' with line start and end points
     * @param t Interpolation given factor 't'
     * @param a Start coordinate value
     * @param b End coordinate value
     * @return Value of corresponding coordinate along line from a to b, given factor 't'
     */
    static double lerp(double t, double a, double b) { return (a + t * (b - a)); }

    /* Calculate interpolatable factor 't', to use in linear interpolation
     * @param at Coordinate value deviating from two extreme points making line
     * @param top Start of interpolating range, scaling edge or row
     * @param bot End of interpolating range, scaling edge or row
     * @return Interpolatable factor 't', to use in linear interpolation
     */
    double getInterpolant(double at, double top, double bot) {
        double lrp = (at-top) / (bot-top);
        return (lrp);
    }

    public double normalize2(double p0[], double p1[])
    {
        double dotproduct = p0[0]*p1[0] + p0[1]*p1[1] + p0[2]*p1[2];
        double normalized = Math.sqrt(dotproduct);

        return normalized;
    }

    public double normalize(double p0[], double p1[]) {
        pr("normalize:  p0:");prVP(p0); pr(" p1:");prVP(p1);

        double dotproduct = p0[0]*p1[0] + p0[1]*p1[1] + p0[2]*p1[2];
        double normalized = Math.sqrt(dotproduct);
        pr("nomalized="+normalized+"  dotproduct="+dotproduct);

        return normalized;
    }

    public double normalize(double pt[]) {
        double nor = Math.sqrt(pt[0]*pt[0] + pt[1]*pt[1] + pt[2]*pt[2]);
        pr("nor="+nor+" point:");prVP(pt);

        return nor;
    }

    // Method production a dot product
    public double dotProduct(double vL[], double vR[]) {
        double dp = vL[0]*vR[0] + vL[1]*vR[1] + vL[2]*vR[2];

        return dp;
    }

    /* Sorts three triangle vertices with increasing y, returns TRUE if reordered
     * @param unsorted Set of triangle vertices unordered with increasing Y
     * @param sorted Set of triangle vertices ordered with increasing Y
     * @return true if out of head->middle->tail order
     */
    public boolean sortTriangle(double unsorted[][], double sorted[][]) {
        int head = 0, mid = 0, tail = 0;

        // First slot at head
        if (unsorted[0][1] <= unsorted[1][1] && unsorted[0][1] <= unsorted[2][1]) {
            head = 0; mid = 2; tail = 1;
            if (unsorted[1][1] <= unsorted[2][1]) {
                mid = 1;
                tail = 2;
            }
        }

        // Second slot is at head
        else if (unsorted[1][1] <= unsorted[0][1] && unsorted[1][1] <= unsorted[2][1]) {
            head = 1;
            if (unsorted[0][1] <= unsorted[2][1]) {mid = 0; tail = 2; }
            else { mid = 2; tail = 0; }
        }

        // Last slot is at head of Y increasing order
        else if (unsorted[2][1] <= unsorted[0][1] && unsorted[2][1] <= unsorted[1][1]) {
            head = 2;
            if (unsorted[0][1] <= unsorted[1][1]) {mid = 0; tail = 1; }
            else { mid = 1; tail = 0; }
        }

        if (DBG_MODE) pr("sortTriangle OUT head="+head+" mid="+mid+" tail="+tail);

        for (int i = 0; i < 6; i++) {
            sorted[0][i] = unsorted[head][i];
            sorted[1][i] = unsorted[mid][i];
            sorted[2][i] = unsorted[tail][i];
        }

        // Check if any changes performed to reorder
        return (head != 0 && mid != 1 && tail != 2);
    }


    /***********************************************************
     * SIMPLE FAUX-SERIALIZATION of ARRAY NUMERICS
     *
     ************************************************************/

    public String toStringFullVertex(double v[]) {
        String s = new String("v={"+fmt.format(v[0])+", "+fmt.format(v[1]));
        s += ", "+fmt.format(v[2])+", "+fmt.format(v[3])+", "+fmt.format(v[4]);

        return (s+", "+fmt.format(v[5])+"}");
    }

    public String toStringVPoint(double v[]) {
        String s = new String("vpoint={"+fmt.format(v[0])+", "+fmt.format(v[1]));

        return (s+", "+fmt.format(v[2])+"}");
    }

    public String toStringVertex(double p[], int range, String prefix) {
        String s = new String(prefix+"{ ");
        for (int i = 0; (i+1) < range; i++) s += fmt.format( p[ i ] )+", ";

        return (s+fmt.format( p[ range-1 ] )+" }");
    }

    public String toStringVertex(int v[], int range, String prefix) {
        String s = new String(prefix+"{ ");
        for (int i = 0; (i+1) < range; i++) s += v[i]+", ";

        return (s+v[ range-1 ]+" }");
    }


    /*************************************************************
     * SERIALIZATION FOR VERTICES, POINTS, COORDINATES, NORMALS,
     * RGB, various numeric arrays, etc.
     *
     * Overloaded liberally for flexibility while generating trace, debugging
     *************************************************************/

    public String toStringV(int v[], String prefix) { return toStringVertex(v, 6, prefix); }
    public String toStringP(int v[], String prefix) { return toStringVertex(v, 3, prefix); }
    public String toStringV(String prefix, int v[]) { return toStringVertex(v, 6, prefix); }
    public String toStringP(String prefix, int v[]) { return toStringVertex(v, 3, prefix); }

    public String toStringV(double v[], String prefix) { return toStringVertex(v, 6, prefix); }
    public String toStringP(double v[], String prefix) { return toStringVertex(v, 3, prefix); }
    public String toStringV(String prefix, double v[]) { return toStringVertex(v, 6, prefix); }
    public String toStringP(String prefix, double v[]) { return toStringVertex(v, 3, prefix); }

    public String toStringP(int v[]) { return toStringVertex(v, 3, " "); }
    public String toStringV(int v[]) { return toStringVertex(v, 6, " "); }
    public String toStringP(double v[]) { return toStringVertex(v, 3, " "); }
    public String toStringV(double v[]) { return toStringVertex(v, 6, " "); }

    public String toStringD(double d) { String s = new String(fmt.format(d)); return s; }


    public void dump3DPoint(double p3[], String msg) { pr(toStringVertex(p3, 3, msg)); }
    public void dumpFullVertex(int v[]) { if ( DBG_MODE ) pr(toStringVertex(v, 6, "v=")); }


    /***********************************************************
     * SIMPLE FAUX-SERIALIZATION of ARRAY NUMERICS
     * - Wrapped to condense code, regulate logging types
     *
     ************************************************************/

    public void pr(String s) { System.err.println(s); }
    public void er(String s) { System.err.println("ERROR: "+s); }
    public void prV(double pv[]) { pr(toStringFullVertex(pv)); }
    public void prVP(double pv[]) { pr(toStringVPoint(pv)); }
    public void prD(double d) { pr(toStringD(d)); }


}


