class Main {
    public static void main(String[] args) {
        // load images into matrix
        ImageLoader imageLoader = new ImageLoader();
        Matrix matrix = imageLoader.loadImages(); // nacteme obrazky (zdrojova data)
        Matrix palette = imageLoader.loadPalette(); // nacteme obrazove body z barevne palety

        // initialize display window
        ImageDisplay imageDisplay = new ImageDisplay();
        imageDisplay.setMatrix(matrix);
        imageDisplay.setPalette(palette);

        // draw display
        imageDisplay.drawDisplay();
    }
}