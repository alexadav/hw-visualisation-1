
/**
 * @brief Phong shading, Phong illumination model
 *
 * @author Francis Kirigin
 *
 * $Id: LightModel.java,v 1.3 2006/12/04 08:11:15 ffk6042 Exp $
 */

///////////////
//
// PHONG NOTES
//
///////////////

 /* Looks better than ordinary gouraud. Bui-Tuong Phong proposed formula:
  *       color = ambient + (cos x) * diffuse + (cos x)^n * specular
  * where
  *    ambient: color of a polygon when it's not hit by light (minimum color that is),
  *    diffuse: original color of the polygon,
  *   specular: color of polygon when hit by a perpendicular light (the maximum color)
  *          x: angle between light vector and normal, allowed change from -90 .. 90 degrees
  *          n: shininess of polygon
  *
  * Q: Why not x allowed to change 0..360 degrees?
  * A: Because when the angle is over 90 degrees, no light hits the polygon and we ought to
  *    use the minimum value ambient. So we must perform a check, and if the angle is not in
  *    required range, we give it the value 90 degrees, and cosine gets the value zero
  *    the color getting the value ambient.
  *
  * SAMPLE CONFIGURATION:
  *  ROTATE rotateX = 0.0
  *         rotateY = 0.0
  *         rotateZ = 0.0
  *  LIGHT  lightX  = 0.0
  *         lightY  = 0.0
  *         lightZ  = 0.0
  *         l_a     = 200.0
  *         l_p    = 500.0
  *
  *  K_a = 0.2   K_d = 0.5  K_s = 25.0
  *
  *
  */

public class LightModel extends MathOps {

    // NORMAL VECTOR
    double normal[] = new double[3];

    // LIGHT SOURCES
    public static final int MAX_SRCS = 4;
    double L[][] = new double[MAX_SRCS][3];
    int nsrcs = 0;

    // REFLECTION VECTOR, EYE POSITION
    double R[] = new double[3];
    double E[] = new double[3], COP[] = new double[3]; // Center of projection: COP

    // COLOR of AMBIENT, DIFFUSE, SPECULAR: Ranging from 0.0 .. 1.0
    double rgbA[] = new double[3], rgbD[] = new double[3], rgbS[] = new double[3];
    double rgbAMod[] = new double[3], rgbDMod[] = new double[3], rgbSMod[] = new double[3];

    double eyeMatrix[][] = new double[4][4];

    // CONSTANTS for AMBIENT, DIFFUSE, SPECULAR COMPONENTS
    double ambient = 0.0, diffuse = 0.0, specular = 0.0;
    int specularPower = 5;

    boolean MODE_SET_EYE = false;


    /*********************************************************
     * CONSTRUCTORS for Light Model to highlight surface
     *
     *********************************************************/

    public LightModel()
    {
        this.initLight();
    }

    public LightModel(double source[], double n[], double a, double d, double s)
    {
        this.initPhong(source, a, d, s);

        for (int i = 0; i < 3; i++)	this.normal[i] = n[i];
    }

    public LightModel(double source[], double a, double d, double s)
    {
        this.initPhong(source, a, d, s);
    }

    public LightModel(double source[])
    {
        this.initLight();
        for (int i = 0; i < 3; i++)	this.L[0][i] = source[i];
    }

    /* Initiates Phong light model, sets raw data parameters. Post initiation of
     * all data structures to 0.0 default 0.0 value, follows with given data
     * to set Primary light source, for Ambient, Diffuse, Specular constants.
     *
     * Phong Shading example constants: Ambient = 0.2, Specular = 0.5, Diffuse = 0.5
     *
     * @param source Light source
     * @param a Ambient constant
     * @param d Diffuse constant
     * @param s Specular constant
     */
    public void initPhong(double source[], double a, double d, double s) {
        this.initLight();

        this.setAmbient(a);
        this.setDiffuse(d);
        this.setSpecular(s);

        for (int i = 0; i < 3; i++) this.L[0][i] = source[i];
    }

    /* Initialize data structures for Phong shading.
     * RAW DATA: Set raw directional vectors with default value 0.0 representing:
     * - Normal vector, Light (L)
     * - PHONG RGB COMPONENTS: Ambient, Diffuse, Specular
     *
     * PHONG CALCULATED COMPONENT VECTORS: Initiate vars, calculated later:
     * - VECTOR COMPONENTS: Reflection (R): (0,0,0)
     *   Eye matrix (E), Center Of Projection (COP): eye position: (0,0,6)
     */
    public void initLight() {
        for (int i = 0; i < 3; i++) {
            this.L[0][i] = 0.0;
            this.COP[i] = this.E[i] = 0.0;
            this.R[i] = 0.0;

            this.normal[i] = 0.0;
            this.rgbA[i] = this.rgbD[i] = this.rgbS[i] = 0.0;
        }

        // Sets as default for viewpoint direction: Z value of 6.0
        this.COP[2] = this.E[2] = 3.0;

        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++) this.eyeMatrix[ i ][ j ] = 0.0;
    }


    /********************************************
     * PHONG SHADING DATA MODIFIERS
     *  - Light sources; Normal direction; Ambient, Diffuse, Specular constants, RGB
     *
     ********************************************/

    public void addSource(double src[]) {
        if (this.nsrcs > MAX_SRCS)
            this.nsrcs = MAX_SRCS;
        else if (this.nsrcs < MAX_SRCS)
            this.nsrcs++;

        for (int i = 0; i < 3; i++) this.L[ this.nsrcs-1 ][ i ] = src[ i ];
    }

    public void setNormal(double nx, double ny, double nz) {
        this.normal[0] = nx;
        this.normal[1] = ny;
        this.normal[2] = nz;
    }

    // Eye matrix as camera position
    public void setEyeMatrix(double e[][]) {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++) this.eyeMatrix[ i ][ j ] = e[ i ][ j ];
    }

    public void setAmbient(double a) {
        this.ambient = a;
        if (this.ambient != 0)
            for (int r = 0; r < 3; r++) this.rgbAMod[r] = this.rgbA[r]*this.ambient;
    }

    public void setDiffuse(double d) {
        this.diffuse = d;
        if (this.diffuse != 0)
            for (int i = 0; i < 3; i++) this.rgbDMod[i] = this.rgbD[i]*d;
    }

    public void setSpecular(double s) {
        this.specular = s;
        if (this.specular != 0)
            for (int i = 0; i < 3; i++) this.rgbSMod[i] = this.rgbS[i] * s;
    }

    public void setSpecularPower(int power) { this.specularPower = power; }

    /* Sets ambient RGB color
     * @param c RBG value of ambient color, should be tinted darker color (dirty green)
     */
    public void setAmbientColor(double c[]) {
        for (int i = 0; i < 3; i++) this.rgbAMod[i] = this.rgbA[i] = c[i];
    }

    public void setDiffuseColor(double c[]) {
        for (int i = 0; i < 3; i++) this.rgbD[i] = c[i];
//        if (this.diffuse != 0) for (int i = 0; i < 3; i++) this.rgbD[i] *= this.diffuse;
    }

    public void setSpecularColor(double c[]) {
        for (int i = 0; i < 3; i++) this.rgbS[i] = c[i];
//        if (this.specular != 0) for (int i = 0; i < 3; i++) this.rgbS[i] *= this.specular;
    }


    /***************************************************
     * CALCULATION OF COMPONENT VECTORS
     * - Ambient, Diffuse, Specular
     *
     ***************************************************/

  /* Calculates the reflection direction:
   *   R = 2( n dotproduct L ) * n - L
   *
   * @param light Light source to use
   * @return Directional vector of reflection
   */
    public double[] getReflectionDir(double light[]) {
        double dotp = 2*(dotProduct(this.normal, light)), rdir[] = {0.0,0.0,0.0};

        for (int i = 0; i < 3; i++)
            this.R[i] = rdir[i] = dotp * this.normal[i] - light[i];
        if (false) pr("getReflect: R="+toStringP(R)+" dotP="+toStringD(dotp)+" lite="+
                toStringP(light)+" normal="+toStringP(this.normal));
        return rdir;
    }

    public double[] buildEyeDir(double eye[][]) {
        for (int i = 0; i < 3; i++)
            this.E[i] = this.eyeMatrix[ i ][ i ] - this.normal[ i ];
        return this.E;
    }

    /* Determines diffuse component to apply
     * @return RGB values to apply as Phong shading for Diffuse Component
     */
    public double[] getDiffuseComponent(double light[]) {
        double dotp = dotProduct(this.normal, light);
        double drgb[] = { this.rgbDMod[0], this.rgbDMod[1], this.rgbDMod[2] };

        if (dotp < 0) dotp = 0;
        for (int i = 0; i < 3; i++)
            drgb[ i ] *= dotp; //this.diffuse;

        return drgb;
    }

    public double[] getDiffuseComponent() { return this.getDiffuseComponent(this.L[0]); }

    /* Calculates the Phong shading Specular Component RGB values
     * [rs,gs,bs]max0(R.E)^p
     * @return RGB values to apply as Phong shading for Specular Component
     */
    public double[] getSpecularComponent() {

        double srgb[] = { this.rgbSMod[0], this.rgbSMod[1], this.rgbSMod[2] };

        double dirRef[] = this.getReflectionDir( this.L[0] );
        double dotp = dotProduct( dirRef, this.E );
        double rhs = 0.001*java.lang.Math.pow(dotp, this.specularPower);

        if (rhs < 0) rhs = 0;
        for (int i = 0; i < 3; i++) srgb[i] *= rhs;

        if (false) pr("getSpecularComponent: SPEC="+toStringP(srgb)+" spec rcalc="+rhs);

        return srgb;
    }

    /* Calculates the Phong shading to add/multiply onto a RGB given a
     * normal directional vector
     *
     * @param nx Normal directional vector on X axis
     * @param ny Normal directional vector on Y axis
     * @param nz Normal directional vector on Z axis
     * @return RGB values to apply as Phong shading for given normal
     */
    public void getPhong(double nx, double ny, double nz, double prgb[]) {
        boolean dbg = false, eyeing = false;
        //      this.buildEyeDir(this.eyeMatrix);    //    this.setEyeDirV();

        this.setNormal(nx, ny, nz);

        this.setAmbientColor(prgb);
        this.setSpecularColor(prgb);
        this.setDiffuseColor(prgb);

        this.setAmbient(this.ambient);
        this.setSpecular(this.specular);
        this.setDiffuse(this.diffuse);

        for (int r = 0; r < 3; r++)
            prgb[r] = this.rgbAMod[r];

        double rgbd[] = getDiffuseComponent(), rgbs[] = getSpecularComponent();

        for (int c = 0; c < 3; c++) prgb[c] += rgbd[c] + rgbs[c];

        if (dbg) pr("   getPhong: PHONG="+toStringP(prgb)+toStringP(rgbd, " DIFF=")+
                toStringP(rgbS, " SPEC=")+toStringP(this.normal, " NORM="));
    }

}


